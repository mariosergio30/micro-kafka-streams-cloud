package demo.kafka.functions;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;


@Configuration
//@EnableKafka
//@EnableKafkaStreams
public class KafkaBasics {

  @Bean
  public Consumer<String>  beanConsumerSimples() {

    return value -> System.out.println("BEAN CONSUMER SIMPLES " + value);

  }


  @Bean
  public Function<String, String> beanFunctionUpper() {
    //public Supplier<Message<String>> supplier() {

    Function<String, String> msg = value -> value.toUpperCase() + " *BEAN Function Upper ";

    // NÃO TEM SENTIDO, POIS SO É EXECUTADO NO LOAD DO BEAN
    System.out.println("BEAN Function PRODUCER: " + msg.toString());

    return msg;

    //Message<String> = MessageBuilder
    //    .withPayload(msg)
    //    .setHeader(KafkaHeaders.MESSAGE_KEY, 88)
    //    .build();
    //};
  }


  @Bean
  public Consumer<String> beanConsumerSimplesUpper() {

    return value -> System.out.println("BEAN CONSUMER SIMPLES UPPER: " + value);

  }

}
