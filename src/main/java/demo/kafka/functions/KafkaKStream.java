package demo.kafka.functions;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;


@Configuration
//@EnableKafka
//@EnableKafkaStreams
public class KafkaKStream {


  @Bean /* consome um Kstream e conta as palavras da frase,
        dica de fase para produzir: event stream amazing modern architecture, used by tech companies, around the world. I think stream tech world amazing
        */
  @SuppressWarnings("unchecked")
  public Consumer<KStream<String, String>> beanKStreamConsumerCountWords() {

    Serde<String> STRING_SERDE = Serdes.String();

    Consumer<KStream<String, String>> consumer =
        (input) -> input
            // SEPARA CADA PALAVRA DA FRASE (ESPAÇOS)
            .flatMapValues(value -> Arrays.asList(value.toUpperCase().split("\\W+")))

            .groupBy((key, word) -> word, Grouped.with(STRING_SERDE, STRING_SERDE))
            .count()
            .toStream()
            .foreach((word, count) -> System.out.println("BEAN KSTREAM CONSUMER: " + word + " -> " + count));

    return consumer;
  }



  /*
  @Bean
  public BiConsumer<KStream<String, String>, KStream<String, String>> transacoes() {
    //public Supplier<Message<String>> supplier() {

    System.out.println("BEAN BIconsumer");

    Serde<String> STRING_SERDE = Serdes.String();

    BiConsumer<KStream<String, String>, KStream<String, String>> biConsumer =
    (value1, value2) -> value1
        .groupBy((key, word) -> word, Grouped.with(STRING_SERDE, STRING_SERDE))
        .count().toStream()
        .foreach((word, count) -> System.out.println("KTable Upper: " + word + " -> " + count));

    return biConsumer;

  }
 */


  /*
    @Bean
  public BiFunction< KStream<String, String>, KStream<String, String>, KStream<String, Transaction>> transacoes() {
    //public Supplier<Message<String>> supplier() {

    System.out.println("BEAN FUNCTION");

    Serde<String> STRING_SERDE = Serdes.String();

    return (value1, value2) -> value1
        .groupBy((key, word) -> word, Grouped.with(STRING_SERDE, STRING_SERDE))
        .count();

  }
  */





/*
  @StreamListener(Sink.INPUT)
  public void process(Message<?> message) {
    Acknowledgment acknowledgment = message.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class);
    if (acknowledgment != null) {
      System.out.println("Acknowledgment provided");
      acknowledgment.acknowledge();
    }
  }
 */




}
