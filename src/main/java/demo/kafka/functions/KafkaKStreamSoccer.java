package demo.kafka.functions;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;
import java.util.function.Function;


@Configuration
//@EnableKafka
//@EnableKafkaStreams
public class KafkaKStreamSoccer {



  @Bean // consome um Kstream, processa e publica
  @SuppressWarnings("unchecked")
  public Function<KStream<String, String>, KStream<String, String>> beanKStreamSoccerCounter() {

    Serde<String> STRING_SERDE = Serdes.String();

    /*
    return value -> value
        .peek((key, word) -> System.out.println("BEAN MyFunction consumer KSTREAM: " + key + " -> " + word))
        .map(KeyValue::new);

    return value -> value
        .peek((key, word) -> System.out.println("BEAN MyFunction consumer KSTREAM: " + key + " -> " + word))
        .mapValues((ValueMapper<String, String>) String::toUpperCase);

    */

    return input -> input
        .peek((key, word) -> System.out.println("BEAN KSTREAM Contador de Gools: " + key + " -> " + word))

        // SEPARA CADA PALAVRA DA FRASE (ESPAÇOS)
        //.flatMapValues(value -> Arrays.asList(value.toUpperCase().split("\\W+")))

        /*
         .map((key, value) -> new KeyValue<>(value, value))
        .groupByKey(Grouped.with(Serdes.String(), Serdes.String()))
        */
        .groupBy((key, word) -> word, Grouped.with(STRING_SERDE, STRING_SERDE))
        //.windowedBy(TimeWindows.of(5000))
        //.count(Materialized.as("wordcount-store"))
        .count().toStream()
        .mapValues((key, value) -> key.toString().concat(" - ").concat(value.toString()));
        //.map((key, value) -> new KeyValue<>(key, value));
        //.map((key, value) -> value);

      /*
        .foreach((word, count) -> System.out.println("BEAN TRANSACTION KSTREAM: " + word + " -> " + count))
        //.map(KeyValue::new);
        //.peek((key, value) -> System.out.println("BEAN MyFunction PROCESSADO KSTREAM: " + key + " -> " + value)
        .map((key, value) -> new KeyValue<>(key, value));
         //.reduce(Long::sum)
        //.toStream());
        */

  }


  @Bean // Encaminha para o cluster B
  public Function<String, String> beanSoccerForward() {

    return   value -> value;
  }

  @Bean
  public Consumer<String> beanSoccerInformation() {

    return value -> System.out.println("*** O resultado dos gools foi encaminhado para o cluster B *** " + value);

  }



}
