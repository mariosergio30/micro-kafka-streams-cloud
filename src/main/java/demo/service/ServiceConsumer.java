package demo.service;

import java.util.function.Consumer;

import demo.model.Product;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component("testServices")
//public class ServiceConsumer implements Consumer<Message<Product>> {
public class ServiceConsumer implements Consumer<Message<String>> {



  @Override
  public void accept(final Message<String> stringMessage) {
//  public void accept(final Message<Product> stringMessage) {
    System.out.println("CLASS CONSUMER " + stringMessage.getPayload().toString());
  }

/*
  @Override
  public void accept(String stringMessage) {
    System.out.println("CLASS CONSUMER " + stringMessage);
  }
*/


}
