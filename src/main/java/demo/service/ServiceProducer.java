package demo.service;


import demo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.kafka.support.KafkaHeaders.MESSAGE_KEY;


@RestController
@RequestMapping("/")  
public class ServiceProducer {

	@Autowired
	private StreamBridge streamBridge;

	public void produce(String msg) {

		System.out.println("Producer....");

		//streamBridge.send("streamBridge-topicX",msg);

		Product product = new Product();
		product.setName("FEIJAO");

    streamBridge.send("testServices-out-0",
        MessageBuilder
            //.withPayload(product)
						.withPayload(msg)
            .setHeader(MESSAGE_KEY,"99".getBytes())
						//.setHeader(MESSAGE_KEY,"99") // erro de serializer
						//.setHeader(MESSAGE_KEY,"99") // assim ocorrer erro
            .build()
    );


	}


}



