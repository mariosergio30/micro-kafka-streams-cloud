package demo.webapi;


import demo.service.ServiceProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.Application;


@RestController
@RequestMapping("/")  
public class Controler {

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);


	//@Autowired
	//private StreamsBuilderFactoryBean streamBuilder;


	@Autowired
	public ServiceProducer serviceProducer;

	@RequestMapping(value="/test-streams", method = RequestMethod.GET)
	public ResponseEntity<String> testStreams(@RequestParam String msg) {

			try {

				serviceProducer.produce(msg);

			  LOG.info("DEMO: PRODUCER CONTROLER");


		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// SUCCESS
		return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(),HttpStatus.OK);
	}




}



